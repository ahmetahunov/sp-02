package ru.ahmetahunov.sp.constant;

public final class AppConst {

	public static final String SALT = "salt";

	public static final String SALT_P = "lfmao125ak-i";

	public static final int CYCLE = 33;

	public static final int CYCLE_P = 10;

}

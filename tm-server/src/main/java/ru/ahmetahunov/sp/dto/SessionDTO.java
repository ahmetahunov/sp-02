package ru.ahmetahunov.sp.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.sp.api.service.IUserService;
import ru.ahmetahunov.sp.entity.Session;
import ru.ahmetahunov.sp.enumerated.Role;
import javax.xml.bind.annotation.XmlType;

@Getter
@Setter
@XmlType
@NoArgsConstructor
public final class SessionDTO extends AbstractEntityDTO {

	@NotNull
	private String userId;

	@Nullable
	private String signature;

	@NotNull
	private Role role;

	private long timestamp = System.currentTimeMillis();

	@NotNull
	public Session transformToSession(@NotNull final IUserService userService) {
		@NotNull final Session session = new Session();
		session.setId(this.id);
		session.setUser(userService.findOne(this.userId));
		session.setSignature(this.signature);
		session.setTimestamp(this.timestamp);
		return session;
	}

}

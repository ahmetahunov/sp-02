package ru.ahmetahunov.sp.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;

public final class ComparatorUtil {

	@NotNull
	public static Sort getComparator(@Nullable final String comparator) {
		if (comparator == null || comparator.isEmpty()) return Sort.by("name");
		switch (comparator.toLowerCase()) {
			case ("creationdate"): return Sort.by("creationDate");
			case ("startdate"): return Sort.by("startDate");
			case ("finishdate"): return Sort.by("finishDate");
			case ("status"): return Sort.by("status");
			default: return Sort.by("name");
		}
	}

}

package ru.ahmetahunov.sp.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.sp.constant.AppConst;
import ru.ahmetahunov.sp.exception.AccessForbiddenException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class PassUtil {

    @NotNull
    public static String getHash(@Nullable final String password) throws AccessForbiddenException {
        if (password == null || password.isEmpty()) throw new AccessForbiddenException("Wrong password");
        return getHash(password, AppConst.SALT_P, AppConst.CYCLE_P);
    }

    @NotNull
    private static String getHash(
            @Nullable final String password,
            @Nullable final String salt,
            final int cycle
    ) throws AccessForbiddenException {
        if (password == null || password.isEmpty()) throw new AccessForbiddenException("Wrong password");
        if (salt == null || salt.isEmpty()) throw new AccessForbiddenException();
        try {
            @NotNull MessageDigest encoder = MessageDigest.getInstance("MD5");
            @NotNull String result = password;
            for (int i = 0; i < cycle; i++) {
                encoder.update((salt + result).getBytes());
                @NotNull byte[] buff = encoder.digest();
                result = new BigInteger(1, buff).toString(16);
                encoder.update((result + salt).getBytes());
                buff = encoder.digest();
                result = new BigInteger(1, buff).toString(16);
            }
            return result;
        } catch (NoSuchAlgorithmException e) {
            throw new AccessForbiddenException(e.getMessage());
        }
    }

}

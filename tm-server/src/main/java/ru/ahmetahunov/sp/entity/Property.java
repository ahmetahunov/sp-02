package ru.ahmetahunov.sp.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public final class Property {

	@NotNull
	private String name;

	@NotNull
	private String value;

}

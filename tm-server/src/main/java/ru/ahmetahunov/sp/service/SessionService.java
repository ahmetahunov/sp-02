package ru.ahmetahunov.sp.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.ahmetahunov.sp.api.repository.SessionRepository;
import ru.ahmetahunov.sp.api.service.ISessionService;
import ru.ahmetahunov.sp.dto.SessionDTO;
import ru.ahmetahunov.sp.entity.Session;
import ru.ahmetahunov.sp.enumerated.Role;
import ru.ahmetahunov.sp.exception.AccessForbiddenException;
import ru.ahmetahunov.sp.util.SessionSignatureUtil;
import java.util.List;

@Transactional
public class SessionService implements ISessionService {

	@Setter
	@NotNull
	private SessionRepository repository;

	@Override
	public Session persist(@Nullable final Session session) {
		if (session == null) return null;
		return repository.save(session);
	}

	@Override
	public Session merge(@Nullable final Session session) {
		if (session == null) return null;
		return repository.save(session);
	}

	@Nullable
	@Override
	public Session findOne(@Nullable final String id) {
		if (id == null || id.isEmpty()) return null;
		return repository.findById(id).orElse(null);
	}

	@NotNull
	@Override
	public List<Session> findAll() {
		return repository.findAll();
	}

	@Override
	public void remove(@Nullable final String id) {
		if (id == null || id.isEmpty()) return;
		repository.deleteById(id);
	}

	@Override
	public void validate(@Nullable final SessionDTO session)
			throws AccessForbiddenException {
		if (session == null) throw new AccessForbiddenException();
		@Nullable final Session checkSession = repository.findById(session.getId()).orElse(null);
		if (checkSession == null || checkSession.getSignature() == null)
			throw new AccessForbiddenException();
		@Nullable final String signature = session.getSignature();
		session.setSignature(null);
		@Nullable final String hash = SessionSignatureUtil.sign(session);
		if (!checkSession.getSignature().equals(hash) || !signature.equals(hash))
			throw new AccessForbiddenException();
		final long existTime = System.currentTimeMillis() - session.getTimestamp();
		if (existTime > 32400000) {
			repository.deleteById(session.getId());
			throw new AccessForbiddenException("Access denied! Session has expired.");
		}
	}

	@Override
	public void validate(@Nullable final SessionDTO session, @NotNull final Role role)
			throws AccessForbiddenException {
		validate(session);
		if (!role.equals(session.getRole())) throw new AccessForbiddenException();
	}

}

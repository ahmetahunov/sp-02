package ru.ahmetahunov.sp.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.ahmetahunov.sp.api.repository.UserRepository;
import ru.ahmetahunov.sp.api.service.IUserService;
import ru.ahmetahunov.sp.entity.User;
import ru.ahmetahunov.sp.enumerated.Role;
import ru.ahmetahunov.sp.exception.AccessForbiddenException;
import ru.ahmetahunov.sp.exception.InterruptedOperationException;
import ru.ahmetahunov.sp.util.PassUtil;
import java.util.List;

@Transactional
public class UserService implements IUserService {

    @Setter
    @NotNull
    private UserRepository repository;

    @Override
    public User persist(@Nullable final User user) throws InterruptedOperationException {
        if (user == null) return null;
        if (user.getLogin().isEmpty()) throw new InterruptedOperationException();
        if (contains(user.getLogin())) throw new InterruptedOperationException("Already exists.");
        return repository.save(user);
    }

    @Override
    public User merge(@Nullable final User user) {
        if (user == null) return null;
        return repository.save(user);
    }

    @Nullable
    @Override
    public User findUser(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        return repository.findUserByLogin(login);
    }

    @Nullable
    @Override
    public User findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.findById(id).orElse(null);
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return repository.findAll();
    }

    @Override
    public void updatePasswordAdmin(@Nullable final String userId, @Nullable final String password)
            throws InterruptedOperationException, AccessForbiddenException {
        if (userId == null || userId.isEmpty())
            throw new InterruptedOperationException("This user does not exist.");
        @Nullable final User user = repository.findById(userId).orElse(null);
        if (user == null) throw new InterruptedOperationException("This user does not exist.");
        @NotNull final String hash = PassUtil.getHash(password);
        user.setPassword(hash);
        repository.save(user);
    }

    @Override
    public void updateRole(@Nullable final String userId, @Nullable final Role role)
            throws InterruptedOperationException {
        if (userId == null || userId.isEmpty()) throw new InterruptedOperationException("This user does not exist.");
        if (role == null) throw new InterruptedOperationException("Unknown role.");
        @Nullable final User user = repository.findById(userId).orElse(null);
        if (user == null) throw new InterruptedOperationException("This user does not exist.");
        user.setRole(role);
        repository.save(user);
    }

    @Override
    public void updatePassword(
            @Nullable final String userId,
            @Nullable final String oldPassword,
            @Nullable final String password
    ) throws AccessForbiddenException, InterruptedOperationException {
        if (userId == null || userId.isEmpty()) throw new AccessForbiddenException("This user does not exist.");
        if (oldPassword == null || oldPassword.isEmpty()) throw new AccessForbiddenException("Wrong password.");
        if (password == null || password.isEmpty()) throw new AccessForbiddenException("Wrong password.");
        @Nullable final User user = repository.findById(userId).orElse(null);
        if (user == null) throw new InterruptedOperationException("This user does not exist.");
        @NotNull final String oldHash = PassUtil.getHash(oldPassword);
        if (!user.getPassword().equals(oldHash)) throw new AccessForbiddenException("Wrong password.");
        @NotNull final String hash = PassUtil.getHash(password);
        user.setPassword(hash);
        repository.save(user);
    }

    @Override
    public void updateLogin(
            @Nullable final String userId,
            @Nullable final String login
    ) throws AccessForbiddenException, InterruptedOperationException {
        if (login == null || login.isEmpty()) throw new InterruptedOperationException("Wrong format login.");
        if (userId == null || userId.isEmpty()) throw new AccessForbiddenException("This user does not exist.");
        if (contains(login)) throw new InterruptedOperationException("Already exists.");
        @Nullable final User user = repository.findById(userId).orElse(null);
        if (user == null) throw new InterruptedOperationException("This user does not exist.");
        user.setLogin(login);
        repository.save(user);
    }

    @Override
    public void remove(@Nullable final String id) throws InterruptedOperationException {
        if (id == null || id.isEmpty()) throw new InterruptedOperationException();
        repository.deleteById(id);
    }

    @Override
    public boolean contains(@Nullable final String login) {
        if (login == null || login.isEmpty()) return true;
        return findUser(login) != null;
    }

}

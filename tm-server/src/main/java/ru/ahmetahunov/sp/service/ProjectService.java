package ru.ahmetahunov.sp.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.ahmetahunov.sp.api.repository.ProjectRepository;
import ru.ahmetahunov.sp.api.service.IProjectService;
import ru.ahmetahunov.sp.entity.Project;
import ru.ahmetahunov.sp.exception.InterruptedOperationException;
import ru.ahmetahunov.sp.util.ComparatorUtil;
import java.util.Collections;
import java.util.List;

@Transactional
public class ProjectService implements IProjectService {

    @Setter
    @NotNull
    private ProjectRepository repository;

    @Override
    public Project persist(@Nullable final Project project) throws InterruptedOperationException {
        if (project == null) throw new InterruptedOperationException();
        if (project.getName().isEmpty()) throw new InterruptedOperationException();
        return repository.save(project);
    }

    @Override
    public Project merge(@Nullable final Project project) throws InterruptedOperationException {
        if (project == null) throw new InterruptedOperationException();
        if (project.getName().isEmpty()) throw new InterruptedOperationException();
        return repository.save(project);
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return repository.findOneByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.findById(id).orElse(null);
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId, @Nullable String comparator) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return repository.findAllByUserId(userId, ComparatorUtil.getComparator(comparator));
    }

    @NotNull
    @Override
    public List<Project> findByName(@Nullable final String userId, @Nullable final String projectName) {
        if (projectName == null || projectName.isEmpty()) return Collections.emptyList();
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return repository.findProjectsByUserIdAndNameContainingIgnoreCase(userId, projectName);
    }

    @NotNull
    @Override
    public List<Project> findByDescription(@Nullable final String userId, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (description == null || description.isEmpty()) return Collections.emptyList();
        return repository.findProjectsByUserIdAndDescriptionContainingIgnoreCase(userId, description);
    }

    @NotNull
    @Override
    public List<Project> findByNameOrDesc(@Nullable final String userId, @Nullable final String searchPhrase) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (searchPhrase == null || searchPhrase.isEmpty()) return Collections.emptyList();
        return repository.findByUserIdAndNameOrDescription(userId, "%" + searchPhrase + "%");
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        repository.deleteByUserIdAndId(userId, projectId);
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        repository.deleteById(id);
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        repository.deleteAllByUserId(userId);
    }

}

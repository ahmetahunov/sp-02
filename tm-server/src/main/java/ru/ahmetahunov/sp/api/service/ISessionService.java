package ru.ahmetahunov.sp.api.service;

import ru.ahmetahunov.sp.dto.SessionDTO;
import ru.ahmetahunov.sp.entity.Session;
import ru.ahmetahunov.sp.enumerated.Role;
import ru.ahmetahunov.sp.exception.AccessForbiddenException;

public interface ISessionService extends IAbstractService<Session> {

	public void validate(SessionDTO session) throws AccessForbiddenException;

	public void validate(SessionDTO session, Role role) throws AccessForbiddenException;

}

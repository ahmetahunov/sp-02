package ru.ahmetahunov.sp.api.endpoint;

import ru.ahmetahunov.sp.exception.AccessForbiddenException;
import ru.ahmetahunov.sp.exception.InterruptedOperationException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface SessionEndpoint {

	@WebMethod
	public String createSession(
			@WebParam(name = "login") String login,
			@WebParam(name = "password") String password
	) throws AccessForbiddenException, InterruptedOperationException;

	@WebMethod
	public void removeSession(@WebParam(name = "token") String token) throws InterruptedOperationException, AccessForbiddenException;

}

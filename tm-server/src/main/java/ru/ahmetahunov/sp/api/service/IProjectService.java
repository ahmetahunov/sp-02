package ru.ahmetahunov.sp.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.sp.entity.Project;
import java.util.List;

public interface IProjectService extends IAbstractService<Project> {

    @Nullable
    public Project findOne(String userId, String projectId);

    @NotNull
    public List<Project> findByName(String userId, String projectName);

    @NotNull
    public List<Project> findByDescription(String userId, String description);

    @NotNull
    public List<Project> findByNameOrDesc(String userId, String searchPhrase);

    @NotNull
    public List<Project> findAll(String userId, String comparator);

    public void removeAll(String userId);

    public void remove(String userId, String projectId);

}

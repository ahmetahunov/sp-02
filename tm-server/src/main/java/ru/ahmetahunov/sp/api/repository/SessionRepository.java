package ru.ahmetahunov.sp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ahmetahunov.sp.entity.Session;

public interface SessionRepository extends JpaRepository<Session, String> {
}

package ru.ahmetahunov.sp.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;
import ru.ahmetahunov.sp.entity.Task;
import javax.persistence.QueryHint;
import java.util.List;

public interface TaskRepository extends JpaRepository<Task, String> {

    @Nullable
    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    public Task findTaskByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    public List<Task> findAllByUserId(@NotNull String userId, @NotNull Sort sort);

    @NotNull
    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    public List<Task> findAllTasksByUserIdAndProjectId(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull Sort comparator
    );

    @NotNull
    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    public List<Task> findTaskByUserIdAndNameContainingIgnoreCase(
            @NotNull String userId,
            @NotNull String name
    );

    @NotNull
    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    public List<Task> findTaskByUserIdAndDescriptionContainingIgnoreCase(
            @NotNull String userId,
            @NotNull String description
    );

    @NotNull
    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    @Query("SELECT t FROM Task t WHERE t.user.id = :userId AND (t.name LIKE :phrase OR t.description LIKE :phrase)")
    public List<Task> findByNameOrDesc(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("phrase") String searchPhrase
    );

    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    public void deleteByUserIdAndId(@NotNull String userId, @NotNull String taskId);

    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    public void deleteAllByUserId(@NotNull String userId);

}

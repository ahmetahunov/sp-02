package ru.ahmetahunov.sp.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.sp.dto.ProjectDTO;
import ru.ahmetahunov.sp.exception.AccessForbiddenException;
import ru.ahmetahunov.sp.exception.InterruptedOperationException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ProjectEndpoint {

	@Nullable
	@WebMethod
	public ProjectDTO createProject(
			@WebParam(name = "token") String token,
			@WebParam(name = "project") ProjectDTO project
	) throws AccessForbiddenException, InterruptedOperationException;

	@Nullable
	@WebMethod
	public ProjectDTO updateProject(
			@WebParam(name = "token") String token,
			@WebParam(name = "project") ProjectDTO project
	) throws AccessForbiddenException, InterruptedOperationException;

	@Nullable
	@WebMethod
	public ProjectDTO findOneProject(
			@WebParam(name = "token") String token,
			@WebParam(name = "projectId") String projectId
	) throws AccessForbiddenException, InterruptedOperationException;

	@NotNull
	@WebMethod
	public List<ProjectDTO> findProjectByName(
			@WebParam(name = "token") String token,
			@WebParam(name = "projectName") String projectName
	) throws AccessForbiddenException, InterruptedOperationException;

	@NotNull
	@WebMethod
	public List<ProjectDTO> findProjectByDescription(
			@WebParam(name = "token") String token,
			@WebParam(name = "description") String description
	) throws AccessForbiddenException, InterruptedOperationException;

	@NotNull
	@WebMethod
	public List<ProjectDTO> findProjectByNameOrDesc(
			@WebParam(name = "token") String token,
			@WebParam(name = "searchPhrase") String searchPhrase
	) throws AccessForbiddenException, InterruptedOperationException;

	@NotNull
	@WebMethod
	public List<ProjectDTO> findAllProjects(
			@WebParam(name = "token") String token,
			@WebParam(name = "comparator") String comparator
	) throws AccessForbiddenException, InterruptedOperationException;

	@WebMethod
	public void removeAllProjects(
			@WebParam(name = "token") String token
	) throws AccessForbiddenException, InterruptedOperationException;

	@WebMethod
	public void removeProject(
			@WebParam(name = "token") String token,
			@WebParam(name = "projectId") String projectId
	) throws AccessForbiddenException, InterruptedOperationException;

}

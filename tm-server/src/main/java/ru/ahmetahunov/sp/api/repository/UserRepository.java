package ru.ahmetahunov.sp.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.QueryHints;
import ru.ahmetahunov.sp.entity.User;

import javax.persistence.QueryHint;

public interface UserRepository extends JpaRepository<User, String> {

    @Nullable
    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    public User findUserByLogin(@NotNull String login);

}

package ru.ahmetahunov.sp.api.service;

import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.sp.entity.User;
import ru.ahmetahunov.sp.enumerated.Role;
import ru.ahmetahunov.sp.exception.AccessForbiddenException;
import ru.ahmetahunov.sp.exception.InterruptedOperationException;

public interface IUserService extends IAbstractService<User> {

    @Nullable
    public User findUser(String login);

    public void updatePasswordAdmin(String userId, String password) throws InterruptedOperationException, AccessForbiddenException;

    public void updateRole(String userId, Role role) throws InterruptedOperationException;

    public void updatePassword(String userId, String oldPassword, String newPassword) throws AccessForbiddenException, InterruptedOperationException;

    public void updateLogin(String userId, String login) throws AccessForbiddenException, InterruptedOperationException;

    public boolean contains(String login);

}

package ru.ahmetahunov.sp.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;
import ru.ahmetahunov.sp.entity.Project;
import javax.persistence.QueryHint;
import java.util.List;

public interface ProjectRepository extends JpaRepository<Project, String> {

    @Nullable
    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    public Project findOneByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    public List<Project> findAllByUserId(@NotNull String userId, @NotNull Sort sort);

    @NotNull
    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    public List<Project> findProjectsByUserIdAndNameContainingIgnoreCase(
            @NotNull String userId,
            @NotNull String name
    );

    @NotNull
    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    public List<Project> findProjectsByUserIdAndDescriptionContainingIgnoreCase(
            @NotNull String userId,
            @NotNull String description
    );

    @NotNull
    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    @Query("SELECT p FROM Project p WHERE p.user.id = :userId AND (p.name LIKE :phrase OR p.description LIKE :phrase)")
    public List<Project> findByUserIdAndNameOrDescription(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("phrase") String searchPhrase
    );

    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    public void deleteAllByUserId(@NotNull String userId);

    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    public void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

}

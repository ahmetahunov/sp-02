package ru.ahmetahunov.sp.exception;

import org.jetbrains.annotations.Nullable;

public final class AccessForbiddenException extends Exception {

	public AccessForbiddenException() { super("Access denied!"); }

	public AccessForbiddenException(@Nullable final String message) {
		super(message);
	}

}

package ru.ahmetahunov.sp.endpoint;

import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.sp.api.endpoint.SessionEndpoint;
import ru.ahmetahunov.sp.api.service.ISessionService;
import ru.ahmetahunov.sp.api.service.IUserService;
import ru.ahmetahunov.sp.dto.SessionDTO;
import ru.ahmetahunov.sp.entity.User;
import ru.ahmetahunov.sp.exception.AccessForbiddenException;
import ru.ahmetahunov.sp.exception.InterruptedOperationException;
import ru.ahmetahunov.sp.util.CipherUtil;
import ru.ahmetahunov.sp.util.PassUtil;
import ru.ahmetahunov.sp.util.SessionSignatureUtil;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;

@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.ahmetahunov.sp.api.endpoint.SessionEndpoint")
public final class SessionEndpointImpl implements SessionEndpoint {

	@NotNull
	private ISessionService sessionService;

	@NotNull
	private IUserService userService;

	@NotNull
	private String secretPhrase;

	@NotNull
	private String endpointHost;

	@NotNull
	private String endpointPort;

	public void init() {
		@NotNull final String host = String.format(
				"http://%s:%s/SessionEndpoint?wsdl",
				endpointHost,
				endpointPort
		);
		Endpoint.publish(host, this);
		System.out.println(host + " is running...");
	}

	@NotNull
	@Override
	@WebMethod
	public String createSession(
			@WebParam(name = "login") final String login,
			@WebParam(name = "password") final String password
	) throws AccessForbiddenException, InterruptedOperationException {
		if (login == null || login.isEmpty()) throw new AccessForbiddenException();
		@Nullable final User user = userService.findUser(login);
		if (user == null || password == null)
			throw new AccessForbiddenException("Access denied. User does not exist.");
		@NotNull final String hash = PassUtil.getHash(password);
		if (!user.getPassword().equals(hash)) throw new AccessForbiddenException("Wrong password");
		@NotNull final SessionDTO sessionDTO = new SessionDTO();
		sessionDTO.setUserId(user.getId());
		sessionDTO.setRole(user.getRole());
		sessionDTO.setSignature(SessionSignatureUtil.sign(sessionDTO));
		sessionService.persist(sessionDTO.transformToSession(userService));
		return CipherUtil.encrypt(sessionDTO, secretPhrase);
	}

	@Override
	@WebMethod
	public void removeSession(@WebParam(name = "session") final String token)
			throws AccessForbiddenException, InterruptedOperationException {
		@NotNull final SessionDTO session = CipherUtil.decrypt(token, secretPhrase);
		sessionService.remove(session.getId());
	}

}

package ru.ahmetahunov.sp.integration;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.ahmetahunov.sp.api.endpoint.*;
import ru.ahmetahunov.sp.endpoint.AdminEndpointImplService;
import ru.ahmetahunov.sp.endpoint.SessionEndpointImplService;
import ru.ahmetahunov.sp.endpoint.UserEndpointImplService;
import java.util.List;

public class AdminEndpointIT {

	@NotNull
	private static final AdminEndpoint adminEndpoint = new AdminEndpointImplService().getAdminEndpointImplPort();

	@NotNull
	private static final UserEndpoint userEndpoint = new UserEndpointImplService().getUserEndpointImplPort();

	@NotNull
	private static final SessionEndpoint sessionEndpoint = new SessionEndpointImplService().getSessionEndpointImplPort();

	@Nullable
	private static UserDTO userTest1;

	@Nullable
	private static UserDTO userTest2;

	@Nullable
	private static String tokenAdmin;

	@Nullable
	private static String tokenUser;

	@BeforeClass
	public static void init() throws Exception {
		tokenAdmin = sessionEndpoint.createSession("admin", "admin");
		tokenUser = sessionEndpoint.createSession("user", "0000");
		userTest1 = userEndpoint.createUser("test1", "test1");
		userTest2 = userEndpoint.createUser("test2", "test2");
	}

	@AfterClass
	public static void clean() throws Exception {
		if (userTest1 != null) adminEndpoint.userRemove(tokenAdmin, userTest1.getId());
		if (userTest2 != null) adminEndpoint.userRemove(tokenAdmin, userTest2.getId());
		sessionEndpoint.removeSession(tokenAdmin);
		sessionEndpoint.removeSession(tokenUser);
	}

	@Test
	public void findAllUsersExceptionTest() {
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.findAllUsers(null)
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.findAllUsers("")
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.findAllUsers(tokenUser)
		);
	}

	@Test
	public void findAllUsersOkTest() throws Exception {
		List<UserDTO> users = adminEndpoint.findAllUsers(tokenAdmin);
		Assert.assertEquals(4, users.size());
	}

	@Test
	public void userUpdatePasswordAdminExceptionTest() {
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userUpdatePasswordAdmin(null, userTest1.getId(), "0000")
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userUpdatePasswordAdmin("", userTest1.getId(), "0000")
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userUpdatePasswordAdmin(tokenUser, userTest1.getId(), "0000")
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userUpdatePasswordAdmin(tokenAdmin, null, "0000")
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userUpdatePasswordAdmin(tokenAdmin, "", "0000")
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userUpdatePasswordAdmin(tokenAdmin, userTest1.getId(), null)
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userUpdatePasswordAdmin(tokenAdmin, userTest1.getId(), "")
		);
	}

	@Test
	public void userUpdatePasswordAdminOkTest() throws Exception {
		adminEndpoint.userUpdatePasswordAdmin(tokenAdmin, userTest1.getId(), "0000");
		Assert.assertThrows(
				Exception.class,
				() -> sessionEndpoint.createSession(userTest1.getLogin(), "test1")
		);
		@Nullable final String tokenTest = sessionEndpoint.createSession(userTest1.getLogin(), "0000");
		Assert.assertNotNull(tokenTest);
	}

	@Test
	public void userRegisterAdminExceptionTest() {
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userRegisterAdmin(null, "test", "0000")
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userRegisterAdmin("", "test", "0000")
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userRegisterAdmin(tokenUser, "test", "0000")
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userRegisterAdmin(tokenUser, null, "0000")
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userRegisterAdmin(tokenUser, "", "0000")
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userRegisterAdmin(tokenUser, "test", null)
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userRegisterAdmin(tokenUser, "test", "")
		);
	}

	@Test
	public void userRegisterAdminOkTest() throws Exception {
		Assert.assertThrows(
				Exception.class,
				() -> sessionEndpoint.createSession("test", "test")
		);
		@NotNull final UserDTO userTest = adminEndpoint.userRegisterAdmin(tokenAdmin, "test", "test");
		Assert.assertEquals("test", userTest.getLogin());
		@Nullable final String tokenTest = sessionEndpoint.createSession("test", "test");
		Assert.assertNotNull(tokenTest);
		adminEndpoint.userRemove(tokenAdmin, userTest.getId());
	}

	@Test
	public void userFindByLoginExceptionTest() {
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userFindByLogin(null, userTest1.getLogin())
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userFindByLogin("", userTest1.getLogin())
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userFindByLogin(tokenUser, userTest1.getLogin())
		);
	}

	@Test
	public void userFindByLoginOkTest() throws Exception {
		@Nullable final UserDTO found = adminEndpoint.userFindByLogin(tokenAdmin, userTest1.getLogin());
		Assert.assertNotNull(found);
		Assert.assertEquals(userTest1.getId(), found.getId());
		Assert.assertEquals(userTest1.getLogin(), found.getLogin());
		Assert.assertEquals(userTest1.getRole(), found.getRole());
		Assert.assertEquals(userTest1.getPassword(), found.getPassword());
	}

	@Test
	public void userFindByLoginNotFoundTest() throws Exception {
		@Nullable UserDTO found = adminEndpoint.userFindByLogin(tokenAdmin, null);
		Assert.assertNull(found);
		found = adminEndpoint.userFindByLogin(tokenAdmin, "");
		Assert.assertNull(found);
		found = adminEndpoint.userFindByLogin(tokenAdmin, "unknown");
		Assert.assertNull(found);
	}

	@Test
	public void userChangeRoleAdminExceptionTest() {
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userChangeRoleAdmin(null, userTest1.getId(), Role.ADMINISTRATOR)
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userChangeRoleAdmin("", userTest1.getId(), Role.ADMINISTRATOR)
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userChangeRoleAdmin(tokenUser, userTest1.getId(), Role.ADMINISTRATOR)
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userChangeRoleAdmin(tokenAdmin, null, Role.ADMINISTRATOR)
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userChangeRoleAdmin(tokenAdmin, "", Role.ADMINISTRATOR)
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userChangeRoleAdmin(tokenAdmin, "unknown", Role.ADMINISTRATOR)
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userChangeRoleAdmin(tokenAdmin, userTest1.getId(), null)
		);
	}

	@Test
	public void userChangeRoleAdminOkTest() throws Exception {
		Assert.assertEquals(userTest1.getRole(), Role.USER);
		adminEndpoint.userChangeRoleAdmin(tokenAdmin, userTest1.getId(), Role.ADMINISTRATOR);
		@Nullable final UserDTO found = adminEndpoint.userFindByLogin(tokenAdmin, userTest1.getLogin());
		Assert.assertNotNull(found);
		Assert.assertEquals(Role.ADMINISTRATOR, found.getRole());
		adminEndpoint.userChangeRoleAdmin(tokenAdmin, userTest1.getId(), Role.USER);
	}

	@Test
	public void userRemoveExceptionTest() {
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userRemove(null, userTest1.getId())
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userRemove("", userTest1.getId())
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userRemove(tokenUser, userTest1.getId())
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userRemove(tokenAdmin, null)
		);
		Assert.assertThrows(
				Exception.class,
				() -> adminEndpoint.userRemove(tokenAdmin, "")
		);
	}

	@Test
	public void userRemoveOkTest() throws Exception {
		@Nullable final UserDTO check = adminEndpoint.userFindByLogin(tokenAdmin, "test");
		Assert.assertNull(check);
		adminEndpoint.userRegisterAdmin(tokenAdmin, "test", "test");
		@Nullable final UserDTO checkRegistered = adminEndpoint.userFindByLogin(tokenAdmin, "test");
		Assert.assertNotNull(checkRegistered);
		adminEndpoint.userRemove(tokenAdmin, checkRegistered.getId());
		@Nullable final UserDTO checkRemoved = adminEndpoint.userFindByLogin(tokenAdmin, "test");
		Assert.assertNull(checkRemoved);
	}

}

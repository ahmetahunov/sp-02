package ru.ahmetahunov.sp.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.sp.api.endpoint.ProjectDTO;
import ru.ahmetahunov.sp.api.endpoint.TaskDTO;
import ru.ahmetahunov.sp.api.endpoint.UserDTO;
import ru.ahmetahunov.sp.exception.FailedOperationException;
import java.util.Date;

public final class InfoUtil {

    @NotNull
    public static String getProjectInfo(@NotNull final ProjectDTO project) {
        @NotNull final StringBuilder sb = new StringBuilder();
        sb.append("Name: ");
        sb.append(project.getName());
        sb.append("\nDescription: ");
        sb.append(project.getDescription());
        sb.append("\nStart date: ");
        @NotNull final Date startDate = project.getStartDate().toGregorianCalendar().getTime();
        @NotNull String date = DateUtil.formatDate(startDate);
        sb.append(date);
        sb.append("\nFinish date: ");
        @NotNull final Date finishDate = project.getFinishDate().toGregorianCalendar().getTime();
        date = DateUtil.formatDate(finishDate);
        sb.append(date);
        sb.append("\nStatus: ");
        sb.append(project.getStatus());
        sb.append("\nID: ");
        sb.append(project.getId());
        return sb.toString();
    }

    @NotNull
    public static String getTaskInfo(@NotNull final TaskDTO task) {
        @NotNull final StringBuilder sb = new StringBuilder();
        sb.append("Name: ");
        sb.append(task.getName());
        sb.append("\nDescription: ");
        sb.append(task.getDescription());
        sb.append("\nStart date: ");
        @NotNull final Date startDate = task.getStartDate().toGregorianCalendar().getTime();
        @NotNull String date = DateUtil.formatDate(startDate);
        sb.append(date);
        sb.append("\nFinish date: ");
        @NotNull final Date finishDate = task.getFinishDate().toGregorianCalendar().getTime();
        date = DateUtil.formatDate(finishDate);
        sb.append(date);
        sb.append("\nStatus: ");
        sb.append(task.getStatus());
        sb.append("\nID: ");
        sb.append(task.getId());
        return sb.toString();
    }

    @NotNull
    public static String getUserInfo(@Nullable final UserDTO user) throws FailedOperationException {
        if (user == null) throw new FailedOperationException("No such user!");
        @NotNull final StringBuilder sb = new StringBuilder();
        sb.append("Login: ");
        sb.append(user.getLogin());
        sb.append("\nUser status: ");
        sb.append(user.getRole());
        sb.append("\nUser id: ");
        sb.append(user.getId());
        return sb.toString();
    }

}

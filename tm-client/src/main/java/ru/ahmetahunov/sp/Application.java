package ru.ahmetahunov.sp;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.ahmetahunov.sp.config.AppConfiguration;

public final class Application {

    public static void main( final String[] args ) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class);
    }

}

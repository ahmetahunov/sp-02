package ru.ahmetahunov.sp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.ahmetahunov.sp.api.endpoint.*;
import ru.ahmetahunov.sp.endpoint.*;

@Configuration
@ComponentScan("ru.ahmetahunov.sp")
public class AppConfiguration {

	@Bean
	public AdminEndpoint adminEndpoint() {
		return new AdminEndpointImplService().getAdminEndpointImplPort();
	}

	@Bean
	public ProjectEndpoint projectEndpoint() {
		return new ProjectEndpointImplService().getProjectEndpointImplPort();
	}

	@Bean
	public TaskEndpoint taskEndpoint() {
		return new TaskEndpointImplService().getTaskEndpointImplPort();
	}

	@Bean
	public UserEndpoint userEndpoint() {
		return new UserEndpointImplService().getUserEndpointImplPort();
	}

	@Bean
	public SessionEndpoint sessionEndpoint() {
		return new SessionEndpointImplService().getSessionEndpointImplPort();
	}

}

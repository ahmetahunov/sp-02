package ru.ahmetahunov.sp.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.*;
import ru.ahmetahunov.sp.command.AbstractCommand;
import ru.ahmetahunov.sp.exception.FailedOperationException;
import ru.ahmetahunov.sp.util.DateUtil;
import java.lang.Exception;

@NoArgsConstructor
@Component("task-create")
public final class TaskCreateCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        terminalService.writeMessage("[TASK CREATE]");
        @NotNull final String projectId = terminalService.getAnswer("Enter project id: ");
        @NotNull final String taskName = terminalService.getAnswer("Please enter task name: ");
        if (taskName.isEmpty()) throw new FailedOperationException("Name cannot be empty.");
        @NotNull final String description = terminalService.getAnswer("Please enter description: ");
        @NotNull final String startDate =
                terminalService.getAnswer("Please enter start date(example: 01.01.2020): ");
        @NotNull final String finishDate =
                terminalService.getAnswer("Please enter finish date(example: 01.01.2020): ");
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(taskName);
        task.setDescription(description);
        task.setStartDate(DateUtil.parseDate(startDate));
        task.setFinishDate(DateUtil.parseDate(finishDate));
        task.setProjectId(projectId);
        taskEndpoint.createTask(session, task);
        terminalService.writeMessage("[OK]");
    }

}
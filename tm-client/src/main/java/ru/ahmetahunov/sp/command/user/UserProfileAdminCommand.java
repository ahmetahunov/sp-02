package ru.ahmetahunov.sp.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.AdminEndpoint;
import ru.ahmetahunov.sp.api.endpoint.UserDTO;
import ru.ahmetahunov.sp.command.AbstractCommand;
import ru.ahmetahunov.sp.util.InfoUtil;

@NoArgsConstructor
@Component("show-user-profile")
public final class UserProfileAdminCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "show-user-profile";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show selected user's profile.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        terminalService.writeMessage("[USER PROFILE]");
        @NotNull final String login = terminalService.getAnswer("Please enter user login: ");
        @Nullable final UserDTO user = adminEndpoint.userFindByLogin(session, login);
        terminalService.writeMessage(InfoUtil.getUserInfo(user));
    }

}

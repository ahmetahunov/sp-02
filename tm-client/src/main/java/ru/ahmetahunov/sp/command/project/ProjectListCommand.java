package ru.ahmetahunov.sp.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.*;
import ru.ahmetahunov.sp.command.AbstractCommand;
import java.lang.Exception;

@NoArgsConstructor
@Component("project-list")
public final class ProjectListCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all available projects";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        int i = 1;
        terminalService.writeMessage("[PROJECT LIST]");
        @NotNull final String comparator = terminalService.getAnswer(
                "Enter sort type<creationDate|startDate|finishDate|status>: "
        );
        for (@NotNull final ProjectDTO project : projectEndpoint.findAllProjects(session, comparator)) {
            terminalService.writeMessage(String.format("%d. %s ID:%s", i++, project.getName(), project.getId()));
        }
    }

}
package ru.ahmetahunov.sp.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.*;
import ru.ahmetahunov.sp.command.AbstractCommand;
import java.lang.Exception;

@NoArgsConstructor
@Component("task-list")
public final class TaskListCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all available tasks.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        terminalService.writeMessage("[TASK LIST]");
        @NotNull final String comparator =
                terminalService.getAnswer("Enter sort type<creationDate|startDate|finishDate|status>: ");
        int i = 1;
        for (@NotNull final TaskDTO task : taskEndpoint.findAllTasks(session, comparator)) {
            terminalService.writeMessage(String.format("%d. %s ID:%s", i++, task.getName(), task.getId()));
        }
    }

}
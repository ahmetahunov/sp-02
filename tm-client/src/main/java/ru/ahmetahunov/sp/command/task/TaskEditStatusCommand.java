package ru.ahmetahunov.sp.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.*;
import ru.ahmetahunov.sp.command.AbstractCommand;
import ru.ahmetahunov.sp.exception.FailedOperationException;
import ru.ahmetahunov.sp.util.StatusUtil;
import java.lang.Exception;

@NoArgsConstructor
@Component("task-edit-status")
public final class TaskEditStatusCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "task-edit-status";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit status of selected task.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        terminalService.writeMessage("[EDIT TASK STATUS]");
        @NotNull final String taskId = terminalService.getAnswer("Please enter task id: ");
        @Nullable final TaskDTO task = taskEndpoint.findOneTask(session, taskId);
        if (task == null) throw new FailedOperationException("Selected task does not exist.");
        @NotNull final String statusName = terminalService.getAnswer("Enter status<planned|in-progress|done>: ");
        @Nullable final Status status = StatusUtil.getStatus(statusName);
        task.setStatus(status);
        taskEndpoint.updateTask(session, task);
        terminalService.writeMessage("[OK]");
    }

}

package ru.ahmetahunov.sp.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.AdminEndpoint;
import ru.ahmetahunov.sp.command.AbstractCommand;
import ru.ahmetahunov.sp.exception.FailedOperationException;

@NoArgsConstructor
@Component("change-user-pass")
public final class UserChangePassAdminCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "change-user-pass";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change selected user's password.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        terminalService.writeMessage("[CHANGE USER'S PASSWORD]");
        @NotNull final String userId = terminalService.getAnswer("Please enter user's id: ");
        @NotNull String password = terminalService.getAnswer("Please enter new password: ");
        @NotNull final String repeatPass = terminalService.getAnswer("Please enter new password one more time: ");
        if (password.isEmpty() || !password.equals(repeatPass))
            throw new FailedOperationException("Passwords do not match!");
        adminEndpoint.userUpdatePasswordAdmin(session, userId, password);
        terminalService.writeMessage("[OK]");
    }

}

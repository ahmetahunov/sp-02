package ru.ahmetahunov.sp.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.ProjectDTO;
import ru.ahmetahunov.sp.api.endpoint.ProjectEndpoint;
import ru.ahmetahunov.sp.command.AbstractCommand;
import ru.ahmetahunov.sp.exception.FailedOperationException;
import ru.ahmetahunov.sp.util.InfoUtil;

@NoArgsConstructor
@Component("project-info")
public final class ProjectDescriptionCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "project-info";
    }

    @NotNull
    @Override
    public String getDescription() { return "Show project's information."; }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        terminalService.writeMessage("[PROJECT-DESCRIPTION]");
        @NotNull final String projectId = terminalService.getAnswer("Please enter project id: ");
        @Nullable final ProjectDTO project = projectEndpoint.findOneProject(session, projectId);
        if (project == null) throw new FailedOperationException("Selected project does not exist.");
        terminalService.writeMessage(InfoUtil.getProjectInfo(project));
    }

}

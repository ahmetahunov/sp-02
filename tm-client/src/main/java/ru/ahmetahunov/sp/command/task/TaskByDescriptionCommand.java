package ru.ahmetahunov.sp.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.*;
import ru.ahmetahunov.sp.command.AbstractCommand;
import java.lang.Exception;
import java.util.List;

@NoArgsConstructor
@Component("task-by-description")
public final class TaskByDescriptionCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "task-by-description";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks with entered part of description.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        terminalService.writeMessage("[TASKS BY DESCRIPTION]");
        @NotNull final String description = terminalService.getAnswer("Please enter task description: ");
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findTaskByDescription(session, description);
        int i = 1;
        for (@NotNull final TaskDTO task : tasks) {
            terminalService.writeMessage(
                    String.format(
                            "%d.%s ID:%s\nDescription:%s",
                            i++,
                            task.getName(),
                            task.getId(),
                            task.getDescription())
            );
        }
    }

}

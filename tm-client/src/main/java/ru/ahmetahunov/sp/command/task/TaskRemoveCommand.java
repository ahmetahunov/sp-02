package ru.ahmetahunov.sp.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.*;
import ru.ahmetahunov.sp.command.AbstractCommand;
import java.lang.Exception;

@NoArgsConstructor
@Component("task-remove")
public final class TaskRemoveCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "task-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        terminalService.writeMessage("[TASK REMOVE]");
        @NotNull final String taskId = terminalService.getAnswer("Enter task id: ");
        taskEndpoint.removeTask(session, taskId);
        terminalService.writeMessage("[OK]");
    }

}
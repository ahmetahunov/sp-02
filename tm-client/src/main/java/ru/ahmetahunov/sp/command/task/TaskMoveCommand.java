package ru.ahmetahunov.sp.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.*;
import ru.ahmetahunov.sp.command.AbstractCommand;
import ru.ahmetahunov.sp.exception.FailedOperationException;
import java.lang.Exception;

@NoArgsConstructor
@Component("task-move")
public final class TaskMoveCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "task-move";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change project for task.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        terminalService.writeMessage("[TASK-MOVE]");
        @NotNull final String taskId = terminalService.getAnswer("Please enter task id: ");
        @Nullable final TaskDTO task = taskEndpoint.findOneTask(session, taskId);
        if (task == null) throw new FailedOperationException("Selected task does not exist.");
        @NotNull final String projectId = terminalService.getAnswer("Please enter new project id: ");
        task.setProjectId(projectId);
        taskEndpoint.updateTask(session, task);
        terminalService.writeMessage("[OK]");
    }

}

package ru.ahmetahunov.sp.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.command.AbstractCommand;

@NoArgsConstructor
@Component("unknown")
public final class UnknownInfoCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "unknown";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Notifies about unknown operation.";
    }

    @Override
    public void execute() {
        terminalService.writeMessage("Unknown operation. Please enter help for help.");
    }

}

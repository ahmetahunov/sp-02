package ru.ahmetahunov.sp.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.UserEndpoint;
import ru.ahmetahunov.sp.command.AbstractCommand;
import ru.ahmetahunov.sp.exception.FailedOperationException;

@NoArgsConstructor
@Component("user-register")
public final class UserRegistrationCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "user-register";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "New user registration.";
    }

    @Override
    public void execute() throws Exception {
        terminalService.writeMessage("[REGISTRATION]");
        @NotNull final String login = terminalService.getAnswer("Please enter login: ");
        @NotNull String password = terminalService.getAnswer("Please enter password: ");
        @NotNull final String repeatPass = terminalService.getAnswer("Please enter new password one more time: ");
        if (password.isEmpty() || !password.equals(repeatPass))
            throw new FailedOperationException("Passwords do not match!");
        userEndpoint.createUser(login, password);
        terminalService.writeMessage("[OK]");
    }

}


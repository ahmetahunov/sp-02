package ru.ahmetahunov.sp.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.*;
import ru.ahmetahunov.sp.command.AbstractCommand;
import java.lang.Exception;
import java.util.List;

@NoArgsConstructor
@Component("project-by-name")
public final class ProjectByNameCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "project-by-name";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects with entered part of name.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        terminalService.writeMessage("[PROJECTS BY NAME]");
        @NotNull final String projectName = terminalService.getAnswer("Please enter project name: ");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findProjectByName(session, projectName);
        int i = 1;
        for (@NotNull final ProjectDTO project : projects) {
            terminalService.writeMessage(String.format("%d.%s ID:%s", i++, project.getName(), project.getId()));
        }
    }

}

package ru.ahmetahunov.sp.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.*;
import ru.ahmetahunov.sp.command.AbstractCommand;
import java.lang.Exception;
import java.util.List;

@NoArgsConstructor
@Component("task-by-name")
public final class TaskByNameCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "task-by-name";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks with entered part of name.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        terminalService.writeMessage("[TASKS BY NAME]");
        @NotNull final String taskName = terminalService.getAnswer("Please enter task name: ");
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findTaskByName(session, taskName);
        int i = 1;
        for (@NotNull final TaskDTO task : tasks) {
            terminalService.writeMessage(String.format("%d.%s ID:%s", i++, task.getName(), task.getId()));
        }
    }

}

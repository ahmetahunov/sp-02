package ru.ahmetahunov.sp.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.SessionEndpoint;
import ru.ahmetahunov.sp.command.AbstractCommand;

@NoArgsConstructor
@Component("exit")
public final class ExitCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Exit from Task Manager.";
    }

    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        if (session != null) {
            sessionEndpoint.removeSession(session);
            stateService.setSession(null);
        }
        terminalService.writeMessage("Have a nice day!");
    }

}
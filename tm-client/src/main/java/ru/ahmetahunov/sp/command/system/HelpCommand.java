package ru.ahmetahunov.sp.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.command.AbstractCommand;

@NoArgsConstructor
@Component("help")
public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all available commands.";
    }

    @Override
    public void execute() {
        for (@NotNull AbstractCommand command : context.getBeansOfType(AbstractCommand.class).values()) {
            terminalService.writeMessage(String.format("%-22s: %s", command.getName(), command.getDescription()));
        }
    }

}
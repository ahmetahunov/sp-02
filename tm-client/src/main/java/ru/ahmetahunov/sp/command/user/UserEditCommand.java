package ru.ahmetahunov.sp.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.endpoint.UserEndpoint;
import ru.ahmetahunov.sp.command.AbstractCommand;
import java.lang.Exception;

@NoArgsConstructor
@Component("user-edit")
public final class UserEditCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Override
    public String getName() {
        return "user-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit user's information.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();
        terminalService.writeMessage("[USER EDIT]");
        @NotNull final String login = terminalService.getAnswer("Please enter new login: ");
        userEndpoint.updateLogin(session, login);
        terminalService.writeMessage("[OK]");
    }

}

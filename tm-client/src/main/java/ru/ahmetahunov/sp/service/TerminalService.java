package ru.ahmetahunov.sp.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.service.ITerminalService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Component
@RequiredArgsConstructor
public final class TerminalService implements ITerminalService {

    @NotNull
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    @NotNull
    @Override
    public String readMessage() throws IOException { return reader.readLine().trim(); }

    @Override
    public void writeMessage(@NotNull final String message) {
        System.out.println(message);
    }

    @NotNull
    @Override
    public String getAnswer(@NotNull final String question) throws IOException {
        System.out.print(question);
        return reader.readLine().trim();
    }

    @Override
    public void close() throws IOException { reader.close(); }

}

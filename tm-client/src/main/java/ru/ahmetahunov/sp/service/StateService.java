package ru.ahmetahunov.sp.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.ahmetahunov.sp.api.service.IStateService;

@Component
@RequiredArgsConstructor
public final class StateService implements IStateService {

    @Getter
    @Setter
    @Nullable
    private String session;

}
